package com.example.simpleconverter.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Scientist {

    private String id;
    private String fullName;
}
