package com.example.simpleconverter.model;

import java.util.Comparator;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Experiment implements Comparator<Experiment> {

    private String project;
    private String id;
    private String scientistId;
    private String result;
    private String temperature;
    private String pressure;
    private String wind;

    @Override
    public int compare(Experiment o1, Experiment o2) {
        return o1.getResult().compareTo(o2.getResult());
    }
}
