package com.example.simpleconverter.model;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProjectsScientists {

    private Map<String, List<Experiment>> projects;
    private Map<String, Scientist> scientists;
}
