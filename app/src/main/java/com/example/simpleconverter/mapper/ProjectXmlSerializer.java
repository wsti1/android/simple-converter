package com.example.simpleconverter.mapper;

import android.util.Xml;

import com.example.simpleconverter.model.Experiment;
import com.example.simpleconverter.model.ProjectsScientists;
import com.example.simpleconverter.model.Scientist;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectXmlSerializer {

    private static final String XML_FEATURE = "http://xmlpull.org/v1/doc/features.html#indent-output";

    private static final String PROJECTS_TAG = "Projekty";
    private static final String PROJECT_TAG = "Projekt";
    private static final String EXPERIMENT_TAG = "Eksperyment";
    private static final String EXPERIMENTERS_TAG = "OsobyProwadzące";
    private static final String EXPERIMENTER_TAG = "Prowadzący";
    private static final String FULL_NAME_TAG = "ImięNazwisko";
    private static final String RESULT_TAG = "Rezultat";
    private static final String TEMPERATURE_TAG = "Temperatura";
    private static final String PRESSURE_TAG = "Ciśnienie";
    private static final String WIND_TAG = "Wiatr";
    private static final String SCIENTIST_ID_ATTRIBUTE = "naukowiec_id";
    private static final String ID_ATTRIBUTE = "id";
    private static final String NAME_ATTRIBUTE = "nazwa";

    public String serialize(ProjectsScientists projectsScientists) {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        try {
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            xmlSerializer.startDocument(StandardCharsets.UTF_8.name(), true);
            xmlSerializer.setFeature(XML_FEATURE, true);

            XmlSerializerDecorator serializer = new XmlSerializerDecorator(xmlSerializer);
            serializeProjects(serializer, projectsScientists.getProjects());
            serializeScientists(serializer, projectsScientists.getScientists());

            xmlSerializer.endDocument();
            xmlSerializer.flush();
            return writer.toString();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to serialize projects and scientists");
        }
    }

    private void serializeProjects(XmlSerializerDecorator serializer, Map<String, List<Experiment>> projects) {
        serializer.startTag(PROJECTS_TAG);
        projects.forEach((project, experiments) -> {
            experiments.sort((o1, o2) -> o1.getResult().compareTo(o2.getResult()));

            serializer.startTag(PROJECT_TAG);
            serializer.attribute(NAME_ATTRIBUTE, project);
            experiments.forEach(experiment -> {
                serializer.startTag(EXPERIMENT_TAG);
                serializer.attribute(ID_ATTRIBUTE, experiment.getId());
                serializer.attribute(SCIENTIST_ID_ATTRIBUTE, experiment.getScientistId());
                serializer.taggedText(RESULT_TAG, experiment.getResult());
                serializer.taggedText(TEMPERATURE_TAG, experiment.getTemperature());
                serializer.taggedText(PRESSURE_TAG, experiment.getPressure());
                serializer.taggedText(WIND_TAG, experiment.getWind());
                serializer.endTag(EXPERIMENT_TAG);
            });
            serializer.endTag(PROJECT_TAG);
        });
        serializer.endTag(PROJECTS_TAG);
    }

    private void serializeScientists(XmlSerializerDecorator serializer, Map<String, Scientist> scientists) {
        List<Scientist> scientistList = new ArrayList<>(scientists.values());
        scientistList.sort((o1, o2) -> Integer.valueOf(o1.getId()).compareTo(Integer.valueOf(o2.getId())));

        serializer.startTag(EXPERIMENTERS_TAG);
        scientistList.forEach(scientist -> {
            serializer.startTag(EXPERIMENTER_TAG);
            serializer.attribute(SCIENTIST_ID_ATTRIBUTE, scientist.getId());
            serializer.taggedText(FULL_NAME_TAG, scientist.getFullName());
            serializer.endTag(EXPERIMENTER_TAG);
        });
        serializer.endTag(EXPERIMENTERS_TAG);
    }
}
