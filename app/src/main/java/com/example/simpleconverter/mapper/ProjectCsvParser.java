package com.example.simpleconverter.mapper;

import com.example.simpleconverter.model.Experiment;
import com.example.simpleconverter.model.ProjectsScientists;
import com.example.simpleconverter.model.Scientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class ProjectCsvParser {

    private static final int EXPERIMENTAL_ID = 0;
    private static final int SCIENTIST_NAME = 1;
    private static final int RESULT = 2;
    private static final int TEMPERATURE = 3;
    private static final int PRESSURE = 4;
    private static final int WIND = 5;
    private static final int PROJECT = 6;

    public ProjectsScientists parse(List<String[]> rows) {
        Map<String, List<Experiment>> projects = new HashMap<>();
        Map<String, Scientist> scientists = new HashMap<>();

        for(String[] data : rows) {
            if (!scientists.containsKey(data[SCIENTIST_NAME])) {
                Scientist scientist = Scientist.builder()
                        .id(String.valueOf(scientists.size() + 1))
                        .fullName(data[SCIENTIST_NAME])
                        .build();

                scientists.put(data[SCIENTIST_NAME], scientist);
            }
            if (!projects.containsKey(data[PROJECT])) {
                projects.put(data[PROJECT], new ArrayList<>());
            }
            Experiment experiment = Experiment.builder()
                    .project(data[PROJECT])
                    .id(data[EXPERIMENTAL_ID])
                    .scientistId(requireNonNull(scientists.get(data[SCIENTIST_NAME])).getId())
                    .result(data[RESULT])
                    .temperature(data[TEMPERATURE])
                    .pressure(data[PRESSURE])
                    .wind(data[WIND])
                    .build();

            List<Experiment> experiments = requireNonNull(projects.get(data[PROJECT]));
            experiments.add(experiment);
        }
        return new ProjectsScientists(projects, scientists);
    }
}
