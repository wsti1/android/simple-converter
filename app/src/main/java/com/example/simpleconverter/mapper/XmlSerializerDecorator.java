package com.example.simpleconverter.mapper;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

@AllArgsConstructor
final class XmlSerializerDecorator {

    private XmlSerializer serializer;

    XmlSerializerDecorator taggedText(String tag, Object text) {
        return startTag(tag).text(text).endTag(tag);
    }

    @SneakyThrows(IOException.class)
    XmlSerializerDecorator startTag(String tag) {
        serializer.startTag("", tag);
        return this;
    }

    @SneakyThrows(IOException.class)
    XmlSerializerDecorator attribute(String attribute, Object text) {
        serializer.attribute("", attribute, text.toString());
        return  this;
    }

    @SneakyThrows(IOException.class)
    XmlSerializerDecorator text(Object text) {
        serializer.text(text.toString());
        return this;
    }

    @SneakyThrows(IOException.class)
    XmlSerializerDecorator endTag(String tag) {
        serializer.endTag("", tag);
        return this;
    }
}
