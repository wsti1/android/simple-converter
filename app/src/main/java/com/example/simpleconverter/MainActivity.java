package com.example.simpleconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.simpleconverter.mapper.ProjectCsvParser;
import com.example.simpleconverter.mapper.ProjectXmlSerializer;
import com.example.simpleconverter.model.ProjectsScientists;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class MainActivity extends AppCompatActivity {

    private final static String CSV_FILENAME = "projects.csv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProjectXmlSerializer xmlSerializer = new ProjectXmlSerializer();
        ProjectCsvParser csvParser = new ProjectCsvParser();

        List<String[]> csvRows = readCsvFile();
        ProjectsScientists projectsScientists = csvParser.parse(csvRows);
        String xmlContent = xmlSerializer.serialize(projectsScientists);

        TextView contentText = findViewById(R.id.content_text);
        contentText.setText(xmlContent);
    }

    private List<String[]> readCsvFile() {
        List<String[]> rows = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(CSV_FILENAME)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                rows.add(data);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read and process CSV file");
        }
        return rows;
    }
}
